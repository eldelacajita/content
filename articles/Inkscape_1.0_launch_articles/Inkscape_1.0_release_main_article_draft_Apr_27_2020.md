# INTRODUCING INKSCAPE 1.0

## Smoother performance, HiDPI support, new & improved Live Path Effects & native macOS app

After a little over three years in development, the team is excited to launch the long awaited [Inkscape 1.0](https://inkscape.org/release/) into the world. 

Built with **the power of a team of volunteers** (*link to interviews/main story*), this open source vector editor represents the work of many hearts and hands from around the world, ensuring that Inkscape remains available free for everyone to download and enjoy. 

In fact, [translations for over 20 of the 88 languages were updated](https://wiki.inkscape.org/wiki/index.php?title=Release_notes/1.0#Translations_.5Bas_of_2019-12-18.5D) for version 1.0, making the software more accessible to people from all over the world.

A major milestone was achieved in enabling Inkscape to use a more recent version of the software used to build the editor's user interface (namely GTK+3). Users with HiDPI (high resolution) screens can thank teamwork that took place during the 2018 [Boston Hackfest](https://inkscape.org/en/news/2018/05/22/2018-boston-inkscape-hackfest/) for setting the updated-GTK wheels in motion.

### Smoother performance & native macOS application

This latest version is available for Windows, Linux and macOS. All macOS users will notice that this latest version is labelled as 'preview', which means that additional improvements are scheduled for the next versions. Overall, 1.0 delivers a smoother, higher performance experience on Windows and Linux, and a better system integration (no more XQuartz!) on macOS.


### So many new bells and whistles

One of the first things users will notice is a reorganized tool box, with a more logical order. There are many new and improved Live Path Effect (LPE) features. The new searchable LPE selection dialog now features a very polished interface, descriptions and even the possibility of marking favorite LPEs. Performance improvements are most noticeable when editing node-heavy objects, using the Objects dialog, and when grouping/ungrouping.

*IMAGE - LPE dialog*


### Canvas flexibility & more for freestyle drawing

Freestyle drawing users can now mirror and rotate the canvas, and test out Xray and Split-view modes. The new PowerPencil provides pressure-dependent width and it is finally possible to create closed paths. Inkscape now allows you to vectorize line drawings, too, in the new unified Trace Bitmap dialog. New path effects that will appeal to the artistic user include Offset, PowerClip and PowerMask LPEs.

*IMAGE split view* 


### Duplicate guides, Corners & Hairlines for technical drawing

Users who work on technical drawings will appreciate being able to create a duplicate guide, aligning grids to the page, the Measure tool's path indicator and the inverted Y-axis, which makes coordinates match between the SVG code and the Inkscape user interface. Potential favorite new LPEs might be **Corners (Fillet/Chamfer)** for even rounding / cutting of path corners, **Ellipse from Points** for construction of circles and ellipses and **Measure Segments** for architectural plans and other real-world object measuring. A new functionality with the Circle Tool means it can create closed arcs (fillets) with the click of a button. When it comes to SVG and CSS, Inkscape 1.0 can make use of SVG2 vector hatches, and can render and export hairlines.

*IMAGE - CORNERS*


### PDF export, text & document fixes for designing

Designers will appreciate being able to export PDFs with clickable links and metadata. They can enjoy new palettes and mesh gradients that work in the web browser, as well as the handy on-canvas alignment for objects. When it comes to wrangling text in Inkscape, variable font support, browser-compatible flowed text, simplified, yet powerful line-height settings will make that a joy. New templates for different screen sizes, margin guides and a colorful checkerboard background are now available. Inkscape 1.0 even features an extension for creating interactive mockups to simulate user interaction with an app in the web browser for presentations to clients and usability testing.  

*IMAGE on-canvas-aligment*


### Customizable themes, icons, fonts & UI

For users interested in customizing their user interface, Inkscape 1.0 allows for plenty of tinkering. From menus and toolbars to page sizes and custom font directories, there is lots to discover. Choose from your installed themes to give Inkscape a dark or bright interface, and select one of the available icon sets, which include customizable single-color icons and the newly-designed multicolor icon set. The dialog for saving the current file as a template, with keywords and title, allows you to always have the template you need available.

*IMAGE - different themes in contrast*


### Some fundamental changes

Extensions have undergone some fundamental changes in version 1.0. Over the years, Inkscape users have become used to working with these third-party extensions, such as various ones used for laser cutting and exporting to file formats, which are not a native part of Inkscape. While outreach to extension developers was undertaken as Inkscape migrates away from Python 2 and towards a more logical and fully tested extensions API (now hosted in a separate [repository](https://gitlab.com/inkscape/extensions)), not all third-party extensions have been brought forward in this Python 3-compatible update yet. This will mean that 1.0 may not allow some users to continue with their normal workflow. 

For more details on specific updates in Inkscape 1.0, check out the [Release Notes](https://wiki.inkscape.org/wiki/index.php?title=Release_notes/1.0#Inkscape_1.0).


### Download Inkscape 1.0 now

Head [here to download Inkscape 1.0](https://inkscape.org/release/inkscape-1.0/?latest=1) for Windows, Linux or macOS. Don't forget to watch the **1.0** **introduction video**(*link to Chris's video*) for a walkthrough of some of the highlights and updates.

While version 1.0 is a project and community milestone, for the volunteer contributors it represents the beginning of new ones on the road to constantly and consistently improving this open-source vector editor. 


### Reach out & join the community

Questions, comments and feedback on Inkscape are welcome. For user support, head to [Inkscape's chat](https://chat.inkscape.org/channel/inkscape_user), where community volunteers field help requests of all sorts. To report bugs, fill out a report directly at the [Inbox](https://gitlab.com/inkscape/inbox/issues/).

From programmers and translators to writers, researchers and artists, the Inkscape community is a friendly, space to learn and share, build skills and meet others interested in open source and vector editing.  Visit Inkscape's [Community Page](https://inkscape.org/community/) for ways to connect.

Draw Freely! 
