Inkscape 1.0 release - April 2020 interview 

*Informations sur la soumission*

*Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)*

*Soumis par Anonyme (non vérifié)*

*sam, 25/04/2020 - 17:17*

*0.0.0.0*

**Contributor Interview: C.Rogers**



Headline 

**"Kindness, understanding, & working with like-minded folks, motivates me most"**



**Please introduce yourself; what is your name and where in the world do you live?**

My name is C.Rogers, and I live in London in the United Kingdom.



**When did you first meet the Inkscape Community? Please explain how you came to join the project.**

I don't remember the first time I met the community. I sort of joined ad-hoc, watching listening, and offering to do random graphics tasks for the project. I originally came to ask for features or make suggestions, report bugs, and chat with people on IRC. A general exchange of knowledge. No expectations, just exchanging favours and good will. I think it was when I got invited to the Inkscape Leeds Hackfest that I really felt like a true part of the project, and got to know some of the folks I'd casually chatted with over the internet.



**What are the areas and/or teams of the project that you contribute to or have contributed to?**

I make videos, write content, file bugs, do various UX design tasks, make stickers and other print media for the project, as well as help people learn and use Inkscape in our various communities, forums, and chatrooms. I also serve on the Inkscape Board, and advocate Inkscape and Free Software to on-board new contributors.



**What motivates you to contribute to the Inkscape project? What do you like about the community?**

Apart from using Inkscape for my professional design work, I also love to share. It's the kindness, the understanding, and working with like-minded folks that motivates me most. The idea that what we do helps millions of people all over the world is very motivating. People in the project are great at offering encouragement and we all serve each other in that same spirit of sharing.



**Please name and explain a few things that you worked on for Inkscape 1.0. Why did you choose to work on those ones?**

My main responsibility is the 1.0 release video, but I've honestly spent more time helping track down and fix last minute bugs, and trying and addressing feedback from users that may influence the whether or not folks will enjoy Inkscape 1.0. I want it to be the best release we can make it, so that's really been my priority. I do hope folks enjoy the video too though. :)



**What's your favorite part of the 1.0 release?**

There are so many great things in the release to play with... I hardly know which is my favourite. I'm going to have to say all the improvements to how the text system works. Lots of great fixes, and some fun things to play with like variable fonts like Decovar - It's really a boon to designers like myself to have advanced features like this.



**If you could wave a magic wand, what one thing would you like to see in the Inkscape project?**

CMYK colour profile support. That includes soft-proofing for print jobs. That would be a big benefit to making Inkscape just a tad better for print media. It's less of a problem now than it used to be, however. Most online print companies will happily print sRGB graphics, converting colours on the fly. Still, for magazines and professional print jobs, it would be nice to convert using Inkscape rather than having to use Scribus for that purpose.



**What would be your best advice to anyone wanting to contribute to the Inkscape project?**

Just come say Hi at https://chat.inkscape.org/channel/general. Introduce yourself, and join the fun. We'll find stuff for you to work on, no matter what your interest is. As long as you have a friendly attitude, and a giving cooperative spirit, you will fit right in. :)



**Is there anything else you'd like to tell us?**

I'd like to thank everyone in the community for the hard work to bring this great software to the widest possible audience. I benefit every day from using it in my work, and am proud to be able to contribute and help others benefit from it as well. If you'd like to see what I do professionally with Free Open Source software, feel free to visit my website: crogersmedia.com.



______________________________

We'd like to tag you on social media if we use quotes from your interview. If you are on any of the following platforms, please add your handle below:

Mastodon:
Twitter:
Facebook: https://www.facebook.com/groups/inkscape.org
Instagram: https://www.instagram.com/crogersmedia/

CONTACT AND CONFIRM / REVIEW ARTICLE: 

How can we contact you for details / clarifications?@c.rogers on chat.inkscape.org