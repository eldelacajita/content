Inkscape 1.0 release - April 2020 interview

*Informations sur la soumission*

*Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)*

*Soumis par Anonyme (non vérifié)*

*dim, 26/04/2020 - 07:36*

*0.0.0.0*



**Contributor Interview: Rania Amina** 

*-- Needs some additional editing!*

Headline 

**"Pinch-to-Zoom will make Inkscape more intuitive for users of touch screen devices"**



**Please introduce yourself; what is your name and where in the world do you live?**

Hi, I'm Rania Amina from Indonesia, while I live in Yogyakarta Special Region. I work in an office that operates in the scope of cooperatives / credit unions named PT Sakti Kinerja Kolaborasindo (https://ptskk.id) as a UI designer. In addition, I also have a small business in graphic design and web development called zectStudio (https://zectstudio.id). Outside of work, I am active enough to contribute to several open source projects, mainly in graphic design.



**When did you first meet the Inkscape Community? Please explain how you came to join the project.**

The first Inkscape community I met was the Indonesian Inkscape Community on Facebook, I forget exactly, but it was around 2014. Long before joining the community, I had used Inkscape, even though I hadn't used it for intense work needs like now.

In 2016, along with a number of community friends, I created a design community that is now quite popular in Indonesia, namely Gimpscape ID. In the beginning, this community was only focused on sharing experiences and copying aids related to the use of the Inkscape and Gimp applications. However, now Gimpscape has turned into a place of discussion about many open source design applications. This community is active on Telegram (https://t.me/gimpscape) and Instagram (as well as the web and YouTube). Gimpscape's slogan is Learn, Share and Inspire. Over the years, Inkscape users in Indonesia have developed quite rapidly.

Through Gimpscape I also met with a number of colleagues whom I later invited to give back to Inkscape. Some of the scope of contributions that I make with friends for example are creating training sessions (both online / offline), providing resources for learning, tutorials (writing and video), on how to make extensions and tools for Inkscape to help their daily work so that they are more efficient.



**What are the areas and/or teams of the project that you contribute to or have contributed to?**

For the past few months I have tried to make an extension for Inkscape with Sofyan (a community friend). The first extension I made was the Gradient Saver, an extension that allows inkscape to store the gradient color palette so that it can be reused in all documents. This may be very trivial, but for some people who work with a lot of gradient colors, it seems like this feature is really needed.

The second extension that I'm working on is Inkporter. At first this Inkporter was just a simple bash script that used the features of the Inkscape CLI to export based on certain IDs. I set the method so that I can bulk / batch export based on a predetermined ID pattern. Because of the many requests, finally Sofyan and I decided to develop this Inkporter as an extension. I'm glad to hear that this extension has helped many friends working in the field of book design, icon creators, design sellers in several market places, and friends of UI designers who use Inkscape as their main application.

Besides working on extensions, in general, in my daily life I usually also help friends who have just migrated to Inkscape through the Gimpscape community. On several occasions we have made **souvenirs about campaigning** for the use of reliable and powerful open source design applications, one of which was Inkscape.



**What motivates you to contribute to the Inkscape project? What do you like about the community?**

Open source is nothing without the spirit of sharing and contributing; that's what I believe. Inkscape has given me many amazing opportunities in life. Meeting friends who have different backgrounds and exchanging ideas, which is a very exciting thing. My biggest motivation in contributing to open source projects is simply to give the best in me while I am given the opportunity to do so.

By helping a lot of other people, we will unwittingly learn, too; and when we learn, the experience and knowledge we have will continue to grow. Wow, this is very exciting for me. At least, contributions are the most likely thing I can do to say "Thank you to Inkscape developers". Maybe we don't know each other yet, but what you have done has helped me a lot. Of course there are many other people out there. That is kindness.



**Please name and explain a few things that you worked on for Inkscape 1.0. Why did you choose to work on those ones?**

I feel that I haven't done much for Inkscape version 1.0 directly. Some time ago when the about screen contest was opened, I participated and was very happy that many people appreciated my art, even though I only managed to reach the runner-up position. That was a positive thing for me.

On the other hand, in this release I only helped a few friends around me who wanted to try the development version by teaching them to compile Inkscape from the source code and invite them to take part in reporting bugs when they find reproductive bugs. This is also quite effective to introduce them about new features in the latest Inkscape later.

When asked why I do that, the answer is very easy. Because that's what I was able to do, and I did it.



**What's your favorite part of the 1.0 release?**

There are many updates in this release; there are some updates that I really like, for example Pinch-to-Zoom. This feature will make Inkscape more intuitive for users of touch screen devices.

The update to the Live Path Effects is also the most amazing thing. LPE Corners (Fillet / Chamfer) is one of the features that I've been waiting for, because this will save me enough time to work on illustrations or mockup interfaces.

And another one is the update to the extension interface. This update will certainly make extensions in inkscape look more modern and intuitive. Thank you to the developers, you guys are amazing!



**If you could wave a magic wand, what one thing would you like to see in the Inkscape project?**

The most basic is the management of application performance. Inkscape is currently very powerful, at least to complete my various jobs, but at certain moments Inkscape still feels heavy even though I use a device with cores and lots of RAM.

One thing, which might be somewhat out of the Inkscape application, but I hope on the gallery page for extensions, there is a feature for version tracking. Another thing that I think is uncomfortable (maybe this is trivial) is the lack of features to save the hashtag when we for example do an update, so inevitably the contributors have to repeat all the filling that has been done before again, not crucial but less efficient.



**What would be your best advice to anyone wanting to contribute to the Inkscape project?**

For anyone who wants to contribute to Inkscape, don't wait for anyone, do even the smallest thing you can do. Inkscape is an application for anyone who loves it and wants to share anything about it.



**Is there anything else you'd like to tell us?**

Once again, thank you to the developers who have taken the time to build Inkscape to this point.



______________________

**We'd like to tag you on social media if we use quotes from your interview. If you are on any of the following platforms, please add your handle below:**

Mastodon: -
Twitter: @raniaamina_id
Facebook: facebook.com/raniaaamina.id
Instagram: @raniaamina.id
Telegram: @raniaamina

How can we contact you for details / clarifications?me@raniaamina.id