Inkscape 1.0 release - April 2020 interview

*Informations sur la soumission*

*Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)*

*Soumis par Anonyme (non vérifié)*

*dim, 26/04/2020 - 11:08*

*0.0.0.0*



**Contributor Interview:  Gellért Gyuris**



Headline 

**"I gave a lecture about Inkscape 1.0 on Software Freedom Day in Hungary"**



**Please introduce yourself; what is your name and where in the world do you live?**

I'm Gellért Gyuris. I am from Szeged, Hungary. I'm 43. I have family, a wife, Erika, 3 daughters and a son: Hanna, Dóra, Blanka, Lehel. I studied Catholic theology, but in my workplace I worked as a frontend developer, which was my hobby. I left this profession after five years and I started working for a small Christian foundation. I led the St. Andrew School of Evangelization project in Hungary for 10 years. This was the place where I first used Inkscape.



**When did you first meet the Inkscape Community? Please explain how you came to join the project.**

When I first met Inkscape it was fully translated (around 2006-2008). Over the years, however, I have found that the translation is unfinished. I was just learning English and I thought it was a good exercise to translate Inkscape from English to Hungarian. But Inkscape did not have a web interface for translation so I had to use Gitlab. It was not easy 😁, but I found the subprojects, I had to ask for a merge request, and I met helpful developers.



**What are the areas and/or teams of the project that you contribute to or have contributed to?**

I contributed mainly to the documentation project (main and tutorials) and I started to work on the main project: tests for command line export functionality.



**What motivates you to contribute to the Inkscape project? What do you like about the community?**

I use free software in my daily work and I have always given something back to this community.



**Please name and explain a few things that you worked on for Inkscape 1.0. Why did you choose to work on those ones?**

1. Translating to Hungarian: interface, man, tutorials, key reference.
2. In the documentation project: I helped with a little content, tests,  and more infrastructure:
   \- made DocBook files valid
   \- real SVG files for web tutorials
   \- new header and footer style for tutorials 
3. Main project: CLI tests
4. I gave a lecture about Inkscape 1.0 on Software Freedom Day in Hungary: https://www.szabadszoftverkonferencia.hu/event/31



**What's your favorite part of the 1.0 release?**

I like the new LPE dialog. I didn't understand anything about that before.



**If you could wave a magic wand, what one thing would you like to see in the Inkscape project?**

Multi page support and CMYK export.



__________________________

**We'd like to tag you on social media if we use quotes from your interview. If you are on any of the following platforms, please add your handle below:**


Facebook: facebook.com/gellert.gyuris

How can we contact you for details / clarifications?gyuris