[Inkscape Watch Party ideas](http://wiki.inkscape.org/wiki/index.php/Inkscape_Classes ) - topics, links and potential hosts / experts

We're seeking to schedule some time to learn together and share our knowledge to advance the project. These sessions will take place on Rocket.Chat and will be promoted ahead of time, including on the wiki page above.

Feel free to share your idea for a watch party here, either in a particular month or in the general ideas space. 

It would be terrific to have the following information ready to add here: 

1. Topic / idea (ex: open source fundraising)
2. One to three links to potential & relevant videos to be shared
3. A suggestion for who on the team could host the watch party




**<u>March 2019</u>** 

Saturday, March 16, 19:00 UTC

1. Topic: **Fundraising for FOSS Projects**  

   https://www.youtube.com/watch?v=TYFdjY63XTQ> - Anatomy of Successful Open Source Communities and Projects (10 min)

   https://www.youtube.com/watch?v=MigSMCc8bmM> - How to raise money for open source projects (25 min)

   <https://www.youtube.com/watch?v=TYFdjY63XTQ> - Fundraising for Projects (61 min)

   Hosts: Bryce & Michèle 







**<u>April 2019</u>**





**<u>May 2019</u>** 





**<u>General Ideas</u>**

