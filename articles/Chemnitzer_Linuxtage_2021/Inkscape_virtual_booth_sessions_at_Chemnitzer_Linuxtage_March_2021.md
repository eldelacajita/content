Inkscape participates in Chemnitzer Linuxtage March 2021



# Demos, Q&A's & more with Inkscape during Chemnitz Linux Days 2021 

If you're curious to learn more about Linux and Inkscape, then you're invited to visit the annual [Chemnitz Linux Days](https://chemnitzer.linux-tage.de/2021/en/) / Chemnitzer Linux-Tage conference taking place on March 13-14, 2021!

Chemnitz Linux Days "stand for peaceful and open minded communication. We promote and welcome an open society from which many new and good ideas emerge," according to the official web site.  

Admission is free and the organizing committee has put together an impressive and fun way to bring participants together on the virtual campus of Chemnitz University of Technology. 

The [CLT Adventure](https://chemnitzer.linux-tage.de/2021/en/addons/clt-adventure), a 2-D game-inspired platform, will enable participants to experience many special adventures and  to meet new people, spend time in the ball-filled pool, play hide and seek underground and hang out in the chill lounge within the four floors.

A team of Inkscape contributors, from different parts of the globe, is preparing to answer questions, demonstrate what Inkscape 1.1 can do. Catch live demos on how to use Inkscape for cutting projects in addition to creating and exporting a PDF. Join us for short presentations on how to use Inkscape as part of a professional workflow and how to get involved in the community. 

Our Inkscape [booth program](https://chemnitzer.linux-tage.de/2021/en/programm/beitrag/205) features presentations in German and English during different times all weekend (times stated in GMT). 

(INSERT GROUP PHOTO HERE)

Catch us at our virtual Inkscape booth, just North of the Entrance in the virtual auditorium building. (INSERT LINK)













