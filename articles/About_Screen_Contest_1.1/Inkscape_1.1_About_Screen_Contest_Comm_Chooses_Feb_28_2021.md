INKSCAPE 1.1 ABOUT SCREEN CONTEST - COMMUNITY CHOOSES TOP 5 ENTRIES

article 



## Congratulations to our Top 5 Inkscape 1.1 About Screen Contest Finalists!



Some **??** Inkscape community voters have chosen the Top 5 Finalists of the version 1.1 About Screen Contest.

These five submissions were chosen from 82 entries by 62 Inkscape artists. Many used [Inkscape 1.1 alpha](https://inkscape.org/release/inkscape-1.1/?latest=1) to create their art, which warms our hearts to see the early release being tested. 

These finalists will now head to the next round of voting by the project's contributors who will choose the final design. We'll announce the winner shortly after the votes have been tallied; voting ends on March 7, 2021.

Congratulations to our Top 5 Finalists! 

1.

2.

3.

4.

5.

Bravo to everyone who submitted an entry! We are thrilled with the time, energy and talent you've shared with Inkscape once again. 

As always, all entries will be considered for different Inkscape promotional visuals, including social media, cover art, web site cover art, and Inkscape hackfest or other project event posters, banners and stickers. 

The next round of voting has begun by the project's contributors who will choose the final winner. The winning design will be part of the version 1.1 build and launch.

Watch for an announcement of the winner and their design here on March 9. We're proud of [our tradition of amazing About Screens throughout the years](https://inkscape.org/community/about-screen-contests/) below the contest rules. Check out our gallery to watch how Inkscape artists have celebrated the projects milestone launches in the past. 

*Interested in contributing to the Inkscape project? Visit us online to find out how you can make a difference by joining our community or donating to support our work!* 



