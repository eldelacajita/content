Due to be published on February 21, 2021, on website to launch voting for Inkscape 1.1 About Screen Contest Vote! 



# Your choice counts! Vote for your favorite Inkscape 1.1 About Screen Contest entry

Vote for your favorite entry in the Inkscape 1.1 About Screen Contest until February 28, 2021.

As the tradition goes, Inkscape invites artists who use our program to help make each new main release shine. Previous About Screen contests – e.g. for versions [0.92](https://inkscape.org/gallery/=about-screen-contest/contest-for-092/) and [1.0](https://inkscape.org/gallery/=about-screen-contest/contest-for-10/) – showed us just how creative our community is!

With the [1.1 alpha release being tested currently](https://inkscape.org/release/inkscape-1.1/?latest=1), we're excited about this important step in the progress of finalizing Inkscape 1.1, due to be launched in Spring 2021.

For  now, we're very proud to present our 1.1 About Contest Entry Gallery, featuring **??? entries, by ?? different Inkscape artists**. Thank you so much to everyone who took the time to create and share your work for the project!

Community voting **opens on February 22 and closes on February 28, 2021**.  
Here are the rules:

- If you don't have a user account on the Inkscape website yet, you need to **[register](https://inkscape.org/user/register/)** to be able to vote.
- Everyone gets 1 vote – this includes all contest participants.
- [**Visit the gallery**](https://inkscape.org/gallery/=about-screen-contest/contest-for-11/) and check out all the amazing works of art.
- Click on one of the thumbnails to see the a larger version of the image and a description. You can also comment on the contest entry.
- To cast your vote, either click on the check mark below the image or hover over the small image in the gallery to see a circle-shaped checkbox you can click on. If you accidentally vote for the wrong entry, you can change your vote by simply voting for a different one.
- Voting will ONLY be accepted via the Inkscape website.
- You can vote until Sunday, February 28, 23:59 (Time zone: UTC).
- Once the community voting is done, the top five entries with the most votes will be passed on to the developers, who will then vote to determine the winner.
- For the contest participants, ANY modifications of the files are prohibited at this point.

Interested in seeing all of the entries that are in the running? **Head here to our [Inkscape Gallery parade page](https://inkscape.org/gallery/=about-screen-contest/contest-for-11/parade/)**.

**Good luck to all who entered!**
