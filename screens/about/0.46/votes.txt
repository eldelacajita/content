Community Voting - Inkscape 0.46 About Screen
Journal Entry: Tue Jan 8, 2008, 6:13 PM
NOTE: There was one entry that was mistakenly overlooked. It is now entry 32. If you would like to change your votes, please do so and it will be taken into account at tally time. I'm so sorry about that. ScislaC

Also noteworthy is that there are two items that did not previously have thumbnails that do now.
--


The contest is officially closed! It looks like it's time to start voting. Following is the list of all entries and who they were by.

The vote will be taken via whomever from dA drops by this journal in the next week as well as any votes cast on our mailing lists by non-da members. Everyone will be choosing their #1 choice and a runner up #2 choice. It will be a point system that is #1 choice = 2 points, and #2 choice = 1 point. All entrants may of course vote as well. Once the preliminary voting is done, the top 3 will be passed on to the developers to have the final vote for the winner.

Given the larger number of submission for this contest, the voting deadline will be extended another 2 days. Voting will begin today, January 7, 2008 and run through January 14, 2008.

Please note that there are a few entries submitted that weren't uploaded to DA, it would be greatly helpful if this could be corrected. Additionally, all SVGs MUST be online before the top 3 is passed on to the developers. If an entry is in the top 3 and does not have an SVG file online at that time, it will be disqualified and the position will be given to the next in line.

Good luck to all who entered, it looks like there's some tough competition!

--

Now, please reply to this journal entry stating which numbers you rate as #1 and #2. Thanks to all those who entered and to those who are voting!

    1. Inkscape 0.46 About Screen by artguy10 - by artguy10
    2. About Inkscape by AV-2 - by AV-2
    3. Versions Ink .46 by valessiobrito - by valessiobrito
    4. INKSCAPE 0.46 SPLASH SCREEN by unicko - by unicko
    5. shinig_star by hrum - by hrum
    6. Stitched Gold by prkos - by prkos
    7. WINNER by needcoffee
    8. CLICK FOR IT - by designstyles (can you please upload to DA so it can be thumbnailed?)
    9. - by archaemic
    10. Hi comunity by Humoram - by Humoram
    11. :thumb73668915: - by dnfjud
    12. INKSCAPE 0.46 SPLASH SCREEN 1 by unicko - by unicko
    13. Inkscape 0.46 about screen by dphase - by dphase
    14. inkscape a propos by grutensaie - by grutensaie
    15. Rubik Ink .46 by valessiobrito - by valessiobrito
    16. :thumb74102587: - by troysobotka
    17. About Screen Contest by haruwen - by haruwen
    18. Inkscape 0.46 Splash by haruwen - by haruwen
    19. :thumb73899923: - by ironya
    20. Inkscape 0.46 About Screen by Miamoto - by Miamoto
    21. :thumb74102521: - by troysobotka
    22. big_city by hrum - by hrum
    23. About Inkscape by jfbarraud - by jfbarraud
    24. inkscape 'about screen' by dambs - by dambs
    25. Inkscape .46 about screen by artguy10 - by artguy10
    26. :thumb73990296: - by AndyFitz
    27. inkscape aboutscreen - sheepie by ryanlerch - by ryanlerch
    28. Inkscape Borg cube by prkos - by prkos
    29. inkscape about screen contest by tekmanjim
    30. Ink Mountain - About 0.46 by ScislaC - by ScislaC
    31. inkscape about-screen by carver-s - by carver-s



211 Comments
:iconinkscapers:
Community Voting - Inkscape 0.46 About Screen
by Inkscapers
Watching
Journals / Personal©2008-2019 Inkscapers
:icondoctormo:
doctormo - Add a Comment:
:iconkarafa1:
Edit
karafa1 Apr 24, 2008
#1 - 7 by needcoffee
#2 - 18 by haruwen
Reply
:iconpebcak:
Edit
pebcak Feb 6, 2008
#1: 7
#2: 11

Die Welt geteilt in gut und schlecht
Und wer bei 10 noch steht, hat Recht.
Reply
:iconmandarancio:
Edit
Mandarancio Feb 4, 2008  Hobbyist General Artist
#1: 23 - by ~jfbarraud
#2: 3 - by ~valessiobrito

There aren't future without freedom, there aren't future without sharing
Reply
:iconorionizer:
Edit
Orionizer Jan 23, 2008
#25
Reply
:iconeverbloom:
Edit
everbloom Jan 23, 2008  Hobbyist General Artist
#1 - 11
#2 - 20

I use :tux:.
Reply
:icondagorath:
Edit
Dagorath Jan 22, 2008  Professional Interface Designer
Number 1: 20.
Number 2: 11.
Reply
:iconjeremified:
Edit
Jeremified Jan 21, 2008
#1 - 20 by =Miamoto.
#2 - 25 by ~artguy10.
Reply
:iconjeremified:
Edit
Jeremified Jan 21, 2008
Whoops, looks like I was way late for the voting. :<
Reply
:iconpadandpen:
Edit
padandpen Jan 21, 2008
my first choice is #13 by dphase
my second choice is #3 by valessiobrito
Reply
:iconcorn13read:
Edit
corn13read Jan 20, 2008
#1 20
#2 10
Reply
:iconbmud:
Edit
bmud Jan 20, 2008   Digital Artist
I vote
#1 | 7
#2 | 26

Osysi = me = [link]
Reply
:iconpsybustermk2:
Edit
psybustermk2 Jan 20, 2008  Hobbyist Digital Artist
#1: 7
#2: 20

Walking on water and developing software from a specification are easy if both are frozen. - Edward V. Berard
Reply
:iconoctan3:
Edit
octan3 Jan 20, 2008
#1 - 18
#2 - 7

Great submissions.
Reply
:icondraxas:
Edit
draxas Jan 20, 2008   Writer
#1 - 14
#2 - 7

98% of Deviants don't know the difference between "your" and "you're." If you're one of the 2% that wants to punch 'em, put this in your sig.
Reply
:iconcorydodt:
Edit
corydodt Jan 19, 2008
#1 = 18
#2 = 8
Reply
:iconpecet:
Edit
PeCet Jan 19, 2008
#1 - 11
#2 - 32
Reply
:iconmarciusarmada:
Edit
MarciusArmada Jan 19, 2008
#1 = 18
#2 = 23
Reply
:iconjohantri:
Edit
johantri Jan 19, 2008
number se7en
Reply
:iconpetriko:
Edit
Petriko Jan 19, 2008
#1 - Entry #1 by artguy10
#2 - Entry #6 by prkos
Reply
:icongeminiguy:
Edit
geminiguy Jan 18, 2008
#1 = #1
#25 = #2

Great submissions!
Reply
:iconwraith83:
Edit
Wraith83 Jan 18, 2008
I vote for:

#1 - Entry#2 by Av-2
#2 - Entry#23 by jfbarraud

Lots of awesome looking ones to choose from, great work everybody.
Reply
:iconmw76:
Edit
mw76 Jan 18, 2008
#1 - 18 - by ~haruwen
#2 - 25 - by ~artguy10
Reply
:iconsuayb:
Edit
Suayb Jan 18, 2008
#12 - by ~unicko
Reply
:iconsuayb:
Edit
Suayb Jan 18, 2008
12 - by ~unicko
Reply
:iconegonpin:
Edit
egonpin Jan 17, 2008  Hobbyist Digital Artist
#1 - 26
#2 - 18
Reply
:iconegonpin:
Edit
egonpin Jan 17, 2008  Hobbyist Digital Artist
#1 - 26
#2 - 18
Reply
:iconegonpin:
Edit
egonpin Jan 17, 2008  Hobbyist Digital Artist
#1 - 26
#2 - 18
Reply
:iconegonpin:
Edit
egonpin Jan 17, 2008  Hobbyist Digital Artist
#1 - 26
#2 - 18
Reply
:iconegonpin:
Edit
egonpin Jan 17, 2008  Hobbyist Digital Artist
#1 - 26
#2 - 18
Reply
:iconegonpin:
Edit
egonpin Jan 17, 2008  Hobbyist Digital Artist
#1 - 26
#2 - 18
Reply
:iconegonpin:
Edit
egonpin Jan 17, 2008  Hobbyist Digital Artist
#1 - 26
#2 - 18
Reply
:iconorijimi:
Edit
orijimi Jan 17, 2008   Digital Artist
First-#7
Second-#3
Reply
:iconfreewally:
Edit
freewally Jan 17, 2008
#1 - 21
#2 - 7

z-index.it

To Start Press Any Key.
"Where's the ANY key?"
[3F05]King-SizeHomer
Reply
:iconicemanflaming:
Edit
icemanflaming Jan 17, 2008   Digital Artist
#1: 13
#2: 11

:bulletred: *vbu :bulletred: *vectorizers :bulletred: ~vector-club :bulletred: ~ILoveYouAsia :bulletred:
Reply
:iconbrianin3d:
Edit
brianin3d Jan 16, 2008  Hobbyist General Artist
14 and 7

"we were born to make [rule 34] a reality"
Reply
:iconpartheeus:
Edit
Partheeus Jan 16, 2008
1. - 7
2. - 14

:du gleichst dem :geist den :du begreifst, nicht mir!!! (:goethe -> :faust)
note: sorry for my english.....
Reply
:icondambs:
Edit
dambs Jan 16, 2008
#1- 24 by dambs
#2- 7 by needcoffee (Really impressive...!)
Reply
:iconamonsha:
Edit
AmonSha Jan 16, 2008
#1 - 7
#2 - 11
Reply
:iconlleroy:
Edit
lleroy Jan 16, 2008
#1 - 25
#2 -1
Reply
:iconanonymous-bot:
Edit
anonymous-bot Jan 16, 2008
#1 - 7

#2 - 18
Reply
:iconwolfprintdg:
Edit
wolfprintDG Jan 15, 2008
1st: 14
2nd: 11
All great... pieces of work

Want the same results.... keep doing the same thing!
Reply
:icontiger-the-lion:
Edit
tiger-the-lion Jan 15, 2008
#1: 25
#2: 31
Reply
:icononeseventeen:
Edit
oneseventeen Jan 15, 2008
1st: 11
2nd: 14

All were good
Reply
:iconsudrien:
Edit
Sudrien Jan 15, 2008
1. 7 - by ~needcoffee
2. 21 - by ~troysobotka

frequent contributor to confusion
Reply
:iconsamej71:
Edit
samej71 Jan 15, 2008
Nice choices.

#1 - 22
#2 - 14
Reply
:iconmanuelgrad:
Edit
ManuelGrad Jan 15, 2008
#1 - 18
#2 - 11

www.manuelgrad.at
Reply
:iconjammykins:
Edit
jammykins Jan 15, 2008
#1 - 23
#2 - 5
Reply
:iconnad2k:
Edit
nad2k Jan 15, 2008
#1 14. by ~grutensaie
#2 32. by ~carver-s
Reply
:iconelaeon:
Edit
elaeon Jan 15, 2008
Is very hard to elect only two, but

#1 - 21
#2 - 11

Good work for all.
Reply
:icondogsthat:
Edit
dogsthat Jan 15, 2008
#1 - 6
#2 - 26

lovely jobs :)
Reply
:iconlpetkov:
Edit
lpetkov Jan 15, 2008  Student Digital Artist
#1 - 20
#2 - 7
Reply
:iconheathbar82:
Edit
heathbar82 Jan 15, 2008
1st: 1
2nd: 18
Reply
:icontene:
Edit
Tene Jan 14, 2008
#1. - 7 by ~needcoffee
#2. - 11 by ~dnfjud
Reply
:icondariorodt:
Edit
dariorodt Jan 14, 2008  Professional Interface Designer
Uff!"""
It's very dificult to chose

#1.- 7 (by needcofee)
#2.- 17 (by haruwen)

Special mention: 23 (by jfbarraud)

North of the south
Reply
:iconkentonr:
Edit
kentonr Jan 14, 2008
#1 - 20
#2 - 7
#3 - 11

They look good.
Reply
:icond135-1r43:
Edit
d135-1r43 Jan 14, 2008
#1: 14
#2: 11
Reply
:icongrutensaie:
Edit
grutensaie Jan 14, 2008
1--> 19 by ironya
2--> 11 by dnfjud

but difficult to choose just two

cycles tempête
Reply
:iconjfbarraud:
Edit
jfbarraud Jan 14, 2008
Argh! I can not make up my mind!! They are all so nice!

#1 = 32 by carver-s; I like the background texture.
#2 = 2 by AV-2; I like the feeling of motion.

31 by Scislac is amazing; too succesfull I would say: I am freezing just looking at the Inkscape logo! 6, 7, 11,17,18,25... Are you sure we can only have one about screen?? :-)
Reply
:iconyoz-y:
Edit
yoz-y Jan 14, 2008
14 as #1
7 as #2
Reply
:iconsmstocking:
Edit
smstocking Jan 14, 2008
#1 - I like 22
#2 - 14
Reply
:iconrauchtmehrobst:
Edit
rauchtmehrobst Jan 14, 2008
#1 -> 11
#2 -> 18
Reply
:iconkapa11:
Edit
kapa11 Jan 14, 2008
1. - by ~artguy10
25.- by ~artguy10
Reply
:iconanarchtic:
Edit
Anarchtic Jan 14, 2008
7
Reply
:iconmcdeesh:
Edit
mcdeesh Jan 14, 2008
#1 -> 7
#2 -> 6
Reply
:icondanielmc:
Edit
danielmc Jan 14, 2008
#1 21
#2 7

great work guys!!!
Reply
:iconlonoak:
Edit
lonoak Jan 14, 2008
#1 12
#2 20
Reply
:iconportnov:
Edit
portnov Jan 14, 2008
#1 - 6
#2 - 10
Reply
:iconpbhj:
Edit
pbhj Jan 14, 2008
1) needcoffee (7)

2) dnfjud (11)

Lots of lovely artwork, inspiring!
Reply
:iconbloud:
Edit
bloud Jan 14, 2008
#1 - 11 (by dnfjud)
#2 - 7 (by needcoffee)
Reply
:icondatatec:
Edit
Datatec Jan 14, 2008
#1 18
#2 1

all the entries are excellent!
Reply
:icongebeleixis:
Edit
gebeleixis Jan 14, 2008
#1--11
#2--21
Reply
:iconinductiveload:
Edit
inductiveload Jan 13, 2008
Hard choice, but the engineer in me says:

#1: 14
#2: 11

The best way to be is to be a little bit crazy. That way, other people are slighty wary of you. I'm not a misanthrope, it's just that I'm slightly wary of other people. They're all a little bit crazy.
Reply
:iconhelix84:
Edit
helix84 Jan 13, 2008
18., 7.

all the submissions are cool. these are the finest and most suitable as an about screen. thanks to all who submitted their pictures.
Reply
:iconalicantist:
Edit
Alicantist Jan 13, 2008
#1 - 20
#2 - 9
Reply
:icontommygam:
Edit
tommygam Jan 13, 2008
Hi world!
my favourite are: 13 - 20

Contratulations to everybody
Tommaso
Reply
:iconninersvk:
Edit
ninersvk Jan 13, 2008
#1 = 20
#2 = 11

nice work guys, like it so much
Reply
:iconacspike:
Edit
acspike Jan 13, 2008
#1 - 7
#2 - 22
Reply
:iconjoelpaula:
Edit
joelpaula Jan 13, 2008
24, by ~dambs
or by ~hrum
Reply
:iconmicrougly:
Edit
microUgly Jan 13, 2008
#1 - 11.
#2 - 32.

I wish we had been able to vote for more than 2 :)

microUgly.com | a Quick Guide to Inkscape | InkscapeForum.com
Reply
:iconboreasank:
Edit
BoreasANK Jan 13, 2008
1# 7
2# 14
3# 32
Reply
:iconbidjibah:
Edit
bidjibah Jan 13, 2008
tough choice...great work.
#1 - 7
#2 - 2
Reply
:iconevil-oatmeal:
Edit
Evil-Oatmeal Jan 13, 2008
#1 - 7
#2 - 11

blarrggghhhh.....
Reply
:iconejnar:
Edit
Ejnar Jan 13, 2008
1#: 7
2#: 24

EJNAR - MUHAHAHAHA...!
Reply
:icondominiquevial:
Edit
DominiqueVial Jan 13, 2008  Professional Traditional Artist
# 1 --> 11
# 2 --> 2

☰
Dominique Vial
www.dominiquevial.fr
Reply
:icondopelover:
Edit
dopelover Jan 13, 2008
#1: 23
#2: 7
Reply
:iconsnapai:
Edit
Snapai Jan 13, 2008  Professional Filmographer
#1: 21
#2: 23
Reply
:iconradishdalek:
Edit
radishdalek Jan 13, 2008  Hobbyist Digital Artist
#1 - 11
#2 - 14

I really need to think up a new comment signature...
Reply
:iconthejf:
Edit
TheJF Jan 13, 2008
#1. 7
#2. 14
Reply
:iconlilith2k3:
Edit
lilith2k3 Jan 13, 2008
1
Reply
:iconsombrerrance:
Edit
SombrErrancE Jan 13, 2008
Hello every body !

There is some good work for sure...

#1 > #21 by troysobotka
#2 > #07 by needcoffee

I'd like to participate to the contest but I don't have the time.
++
Reply
:iconlapega:
Edit
lapega Jan 13, 2008
#1 - 7
#2 - 21

Can be changed the font in 'Draw freely' (#7)?, for something more minimalistic o futuristic?
Reply
:iconbutonic:
Edit
butonic Jan 13, 2008
#1 - 27
#2 - 22
Reply
:iconjschalken:
Edit
JSchalken Jan 13, 2008
#1 - #11 - by ~dnfjud
#2 - #20 - by =Miamoto
Reply
:iconscotman:
Edit
scotman Jan 13, 2008
#1 - 7
#2 - 11

Good work guys!
Reply
:icondreadknight666:
Edit
DreadKnight666 Jan 13, 2008  Professional Digital Artist
#1 - 13 [link])
#2 - 23 ([link])


Lots of great entries, i've had a great time picking just 2.

Congrats to all of the participants, including the devs! :D

The best strategy game and we're making it together! :la: Ancient-Beast
Reply
:iconleonedesign:
Edit
LeoneDesign Jan 13, 2008
#1: 18
#2: 17

Simple, yet very good. Nicely done all of you!
Reply
:iconrugby471:
Edit
rugby471 Jan 13, 2008
First choice - #13
Second choice - #7
Reply
:iconloffe87:
Edit
Loffe87 Jan 13, 2008
#1 - 24 by dambs
#2 - 20 by Miamoto
Reply
:iconmeedio:
Edit
meedio Jan 13, 2008   Digital Artist
#1: 11
#2: 26
Reply
:iconoxben:
Edit
oxben Jan 13, 2008
#1 - 23
#2 - 18
Reply

    Prev
    1
    2
    3
    Next

:icondoctormo:
doctormo - Add a Comment:
Sta.sh Apps

        Loading…

:iconinkscapers: More from Inkscapers
"
Inkscape 1.0 - About screen contest time!
The contest for the much-awaited Inkscape 1.0 has just begun and
we're looking forward to your submissions!
Grab the current Beta version or even a daily build
of the latest code directly from the development pipeline for your operating system, and
show everyone what amazing things you and the new version of your favorite vector graphics editor are capable of creating!
To find out more about what's new, take a peek at the release notes draft for 1.0!
Important Dates (all times in UTC/GMT):
Submission period:
Mon, 7 October 2019 – Sun, 17 November 2019
Community Voting period:
Mon, 18 November 2019 – Sun, 1 December 2019
Final Voting period:
Mon, 2 December 2019 – Sun, 15 December 2019
Winners announced:
17 December 2019
We hope to see a great variety of entries, with lots of cool ideas and neat tricks.
For more details on the contest, follow this link:
https://inkscape.org/community/about-screen-contests/"
"
Inkscape.org launches new *official* forum
For widening the inkscape user community, answering technical questions and showing off personal projects.
Anything about the journey on the ink mountains.
This is a brand new place, first of its kind.
Being run on the official site, that's the closest one can get to developers -hoping they will chime in as well!
All it takes is a registration to inkscape.org then the forum is available here:
https://inkscape.org/forums/
With each member account comes an ad-free gallery, where you can host svg files too."
"
Inkscape launches versions 0.92.4 and 1.0 alpha!
That's right, we are moving in giant steps.
The download page is available here:
https://inkscape.org/release/inkscape-0.92.4/?fbclid=IwAR1-hVfWeX_E-MDPbvufHfqnRXpYi5M9llE9muZsqscuVhMRSbz589O1gL8
Let's see some of the highlights.
•speeeeeeed -no lagging on deselecting paths
•faster filter rendering
•bugfix, bugfix everywhere
•more align and distribute options
•printing the actual size is working with most common printers -Canon, EPSON, Konica Minolta
Important notes:
This new version is the first to drop Windows XP support to the past. Sorry guys if you missed out.
On the other hand, Mac users building with Homebrew can enjoy the latest build with an up-to-date poppler library 0.72.0.
See the rest in the release notes:
https://inkscape.org/release/inkscape-0.92.4/?fbclid=IwAR1-hVfWeX_E-MDPbvufHfqnRXpYi5M9llE9muZsqscuVhMRSbz589O1gL8
Also, reporting bugs is migrated from launchpad to gitlab.
If you found an issue or have a feature request, don't hesitate to"
"
Inkscape Hackfest Kiel 2018 - What Happened?
Besides the developer team having a meetup and building personal relationships?
Some serious work being done.
Just a few of the technical achievements amongst bug fixes and performance boosting:
• Centerline tracing - first steps for inclusion in Inkscape. There is no graphical user interface for it yet, but one step at a time.
•  External SVG linking with real-time changeable resolution.
• A fix for Inkscape's coordinate issues. Coordinate system will flip back to the svg specs's.
Read the rest here:
https://inkscape.org/en/news/2018/09/20/inkscape-hackfest-kiel-2018-what-happened/
A summarising video of the hackfest is in post production, stay tuned."
"
Inkscape's Hackfest goes to Kiel, Germany, in Sept
Wait, what?
Yes, that's right -this year, the development team is having another meetup live in Germany.
Why? Because the Boston Hackfest was so successful it's been decided the next major release will be the long awaited 1.0!
The brainstorming session will be held for five days, where community members are welcomed as well as core developers.
If you'd like to contribute with your presence or with a small donations, check out this link for further details:
https://inkscape.org/en/news/2018/08/01/inkscape-hackfest-germany/"
"
2018 Boston Inkscape Hackfest Overview
Thanks to the generous donations of our community and the hospitality of Red Hat in Boston USA, project contributors from around the world were able to work together for 5 days to make Inkscape better and more accessible.
Read More: https://inkscape.org/en/news/2018/05/22/2018-boston-inkscape-hackfest/
Thanks to everyone from deviantArt who helped Inkscape along to it's next version!"
"
Inkscape 0.92.3 Is Now Available!
Starting up faster. Crashing less. Still drawing freely.
Inkscape 0.92.3 is mainly a stability and bugfix release, but it also brings some small new features, like being able to set an ellipses' radii numerically in the tool controls, or switching the writing direction from left-to-right to right-to-left. Windows users will be happy to learn that the long startup times many of them were seeing could greatly be reduced. A new rendering option with an adjusted default value can vastly improve performance when working with filters.
Many of the bug fixes address important functionality, like printing issues, crashes with the node tool or problems with keyboard shortcuts.
https://inkscape.org/en/news/2018/03/22/announcing-0923-release-inkscape/
For a more detailed listing of what's been fixed, see:
https://inkscape.org/en/releases/0.92.3/
Download link available here:
https://inkscape.org/release/0.92.3/
-Just a note that at this moment the homepage is dealing with a larger traffic, please"
"
Hackfest 2018
Hackfest: Boston/USA, RedHat Boston Office, 26-30 March 2018
A hackfest! We will work on Inkscape bugs, new features, and packaging as well as website issues; in fact anything that can benefit from developers and users being physically in the same space.
The timing of the hackfest will be next to the Software Freedom Day conference held at MIT.
The hackfest will take place at RedHat's office at 300 A Street, which is located in the harbour district, a walkable distance from the very center of town.
RedHat's office is also a short walk from South Station, one of the main MBTA mass transit stops.
For further details, please visit:
http://wiki.inkscape.org/wiki/index.php/Hackfest2018"
"
Inktober with Inkscape
Inkscape is a great tool for creating vectorized versions of sketches. The Inktober event every year is a challenge of drawing and inking artworks, based on a certain set of ideas which you can then post to show off your amazing skills and keep practising your art. We've put together a gallery where you can post your InkTober artworks as well as a few how to videos for ways you can use inkscape in the creation of inked vectors of sketches.
https://inkscape.org/en/gallery/=artwork/inktober-2017/
Can't wait to see all your artworks.
If there's enough demand for it, I can create a gallery here on deviantArt Inkscapers too, let me know."
View Gallery
More from DeviantArt
"
Varyon genos! New Tyrs
:new: All genos can now be exchanged for one Purity trial image each! Likewise for rite images for another arpg as well.
This is where I'll pop all of the genos or imports I have up for grabs!
- USD payments are to be paid via invoice right away.
- Maximum hold of 1 month for art payments, if payment is not completed in that time the geno will be put back up for sale. And all progress will be kept.
- There MUST be progress within the first 2 weeks for art payments or the hold will be removed.
- I can refuse a sale for any reason.
Accepted payments:
- USD
- Points
- Quests/CP/PTs/Training/ICQ/Etc
- Trades
Ping:
Xe-Li - Female Tobiano
Common - Uncommon - Rare - Very rare
Genos
https://www.deviantart.com/comments/1/812035764/4772540754
2) (Female - Tyrian - Empyrian - Healthy)
F: Lisse Coat
T: Dumbo Ears, Veil Tail, Round Eyes
P: Oriental Peanut with Pangare
G: PeN+Or/nPn
$15/3 quests/30CP
https://www.deviantar"
"
[OPEN] FullColor Commission
Hello, I'm Nabakisan, a digital artist that open a commission.
i have a lot to offer so take your time and have a look~ i hope you find something that you like
before commissioning me please read my TOS and my Rules(What I can't/won't draw)
FULLCOLOR STYLE
HEADSHOT
 
 

Price : 13$
Additional Character : +11$
HALFBODY
  
Price : 15$
Additional Character : +13$
KNEEUP
    
Price : 17$
Additional Character : +15$
FULLBODY
 1 by NabakiSan"
"
Commission Info (closed)


SketchBullet; Red 
FlatcolorBullet; Red 
ShadingBullet; Red 
ChibiBullet; Red 
Icon
Static:Bullet; Red 
Bouncy: Bullet; Red 
Pagedoll
static: Bullet; Red 
blinking: Bullet; Red 
Read the rules what shown in the deviantion"
"
[NEW contest] draw my sona #3 ($400 in prizes)
EDIT: PRIZES ALL RAISED!! + Deadline extended by a month. Deadline is now permanent/set in stone
hello! third draw my sona contest, this is my autumn/winter one!!!
regarding #2's contest I hosted in the spring/summer: the prizes are gonna be sent out at the end of the month (september 30th)! and winners will be revealed then too ofc! ♥
Pastel Yellow Star Bullet who to draw Pastel Yellow Star Bullet 
https://toyhou.se/809677.arthur-artie-
https://f2.toyhou.se/file/f2-toyhou-se/watermarks/2943882_s2VmYh1hA.png?1570125747

(people who drew toby/the past oc are NOT disqualified nor have less of a chance of winning!)
Pastel Yellow Star Bullet drawing instructi"
"
pixel commissions - open- Halloween sale!


Update! Spoopy month is here so i'll do a sale during this entire month (you can find a journal with info about my situation on my page). Sale is only available via PayPal payments, points payments will remain the same 

is a lot better is you order more than one icon because of PayPal fees
Point Right Terms of Service Point Left 
Note/comment form
Payment: points/paypal
Commission code: 
References:
Outline color (if wanted):
Extra(s):
Code: icon A
$5,00 usd or 600 :points:
Wizard Hat, complete with Wizard Fire! Poof! $3,00 usdWizard Hat, complete with Wizard Fire! Poof!
"
"
Rant about the lupisvulpes community
Disclaimer

Nothing here is to cause drama or directed at anyone specific, i want people to be AWARE of their actions and how shitty people treat each other in this community. This isn't against Lupis herself i respect her as an artist and she hasn't done anything bad to hurt people etc. This isnt hate on lup, this is more of a rant of her community. THIS IS MY EXPERIENCE WITHIN THIS COMMUNITY.

Please note: this isnt gonna be fancy shit, im bad with grammar and writing im like 2 yrs old

By far this has been one of the worst communities i have ever came across, all the people in this community are so hateful and greedy and with absolutely back stab and hurt people just to get a digital dog. 
Im gonna start with how i go involved with this community. In mid 2017 i was given a Lup design as a gift, i had a friend who was well known who owned half of the more ""popular"" characters at the time so all i heard was drama back and forth about it, i wan"
"
Commissions .:OPEN:.
Please note;
Due to personal reasons, Points only (will accept paypal again in future)
I'm still experimenting with my style, especially chibi, icons/pixels and humans
I do not follow in order (So I might do simpler commissions first)DO NOT GIVE ME OVER WATERMARKED REFERENCES! They are hard to look at and work with especially of they cover markings/colours!!
I have the right to refuse of doing your commission if I don't feel up to itIt might take from 1week to 3months for me to finish your commissionPlease don't rush me UNLESS it is a gift for someone and it has to be done for a special day (in that case please specify what day)
Don't send the payment unless I tell you toYou may ask to be noted when I open commissionsPlease try to give me canine/feline alike speciesCheck my gallery for more recent examples
PLEASE CHECK WHAT IS AND WHAT ISN'T OPEN
I will draw:
- Canine
- Feline
- Feral
- Anthro
I will try to draw:
- Hu"
"
Autumn Lights Event Hub
main page || prompts || rules and reqs checklist
prize claims || faq

autumn lights event 
Welcome to Autumn Lights Event! This is a fun, casual event that allows you to create your own narrative through a series of autumn themed prompts. Mysterious festivals have emerged around the globe, and your esk are invited to participate in various tricks and treats. An introduction prompt must be completed in order to gain access to six prompts, which can be completed at any order or not at all.
Fall leaf - F2U This event is open October 10th - November 20th Fall leaf - F2U
All event artwork and writing must be submitted to"
"
Commissions are OPEN!
Commissions are open!! 
You can commission me through my website at philchoart.com/commissions-inf…
Or simply e-mail me at philcho@yahoo.com about an interest, price quote, or inquiry.
I'd also like to remind you all that I'm open to doing more than the examples below. If you have an idea for a commission, and unsure where it would fall under the prices below, just ask, and I will work with you on the price.
Commission Examples:
Head busts: $30


Layla Headbust Commission by phil-choOn Keys, IMOGEN commission by phil-cho

Single character design/portrait:"
Browse More Like This · Shop Similar Prints
Featured in Groups
:iconinkscapers: Inkscapers
Inkscape Community
Show all (1)
Details

Submitted on
    January 8, 2008
File Size
    0 bytes

Link
Thumb
Stats

Views
    4,747
Favourites
    0
Comments
    211

©2019 DeviantArt. All rights reserved

    About Contact Developers Careers Help & FAQ Advertise Core Members Etiquette Privacy Policy Terms of Service Copyright Policy 


